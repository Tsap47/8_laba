﻿
#include <iostream>
const int N = 10;
using namespace std;

int main()
{

    int n, d, v, h;
    cin >> n;
    float* mas = new float[N];
    float* pervoe = new float[N];
    float* sum = new float[N];
    for (int i = 0; i < n; i++)
    {
        cin >> mas[i];
        d = mas[i];
        sum[i] = 0;
        while (d > 0)
        {
            sum[i] += d % 10;
            d /= 10;
        }
        v = mas[i];
        while ((v / 10) > 0)
        {
            pervoe[i] = v % 10;
            v = v / 10;
        }
    }
    for (int i = 0; i < n; i++)
    {
        for (int j = i + 1; j < n; j++)
        {
            if (mas[i] > mas[j])
            {
                swap(mas[i], mas[j]);
                swap(sum[i], sum[j]);
                swap(pervoe[i], pervoe[j]);
            }
        }
    }
    for (int i = 0; i < n; i++)
    {
        for (int j = i + 1; j < n; j++)
        {
            if (pervoe[i] > pervoe[j])
            {
                swap(mas[i], mas[j]);
                swap(sum[i], sum[j]);
                swap(pervoe[i], pervoe[j]);
            }
        }
    }
    for (int i = 0; i < n; i++)
    {
        for (int j = i + 1; j < n; j++)
        {
            if (sum[i] > sum[j])
            {
                swap(mas[i], mas[j]);
                swap(sum[i], sum[j]);
                swap(pervoe[i], pervoe[j]);
            }
        }
    }
    delete [] pervoe;
    delete [] sum;

    for (int i = 0; i < n; i++)
    {
        cout << mas[i] << "  ";
    }
    return 0;
    delete [] mas;
}

